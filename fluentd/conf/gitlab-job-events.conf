# This configuration file will handle the transformation of build, test, deploy etc events sent to Gitlab. We transform the event into a common structure that we use in all fluentd configurations. This makes it possible to easily filter by a specific property in Kibana and correlate different types of events.

<filter gitlab-event-listener.build>
  type flatten_hash
  separator .
</filter>

<filter gitlab-event-listener.build>
  @type record_transformer
  <record>
    event.origin "gitlab"
    event.name ${record["payload.build_stage"]}
    # git.repository.namespace ${record["Actor.Attributes.logging.params.git.repository.namespace"]}
    git.repository.name ${record["payload.repository.name"]}
    git.branch.name ${record["payload.ref"]}
    git.commit.id ${record["payload.commit.sha"]}
    git.commit.message ${record["payload.commit.message"]}
    git.commit.author.name ${record["payload.commit.author_name"]}
    git.commit.author.email ${record["payload.commit.author_email"]}
    ci.build.id ${record["payload.build_id"]}
    ci.build.stage ${record["payload.build_stage"]}
    ci.build.status ${record["payload.build_status"]}
    ci.build.started_at ${record["payload.build_started_at"]}
    ci.build.finished_at ${record["payload.build_finished_at"]}
    ci.build.duration ${record["payload.build_duration"]}
    log Gitlab CI ${record["payload.build_stage"]} job ${record["payload.build_status"]}
  </record>
</filter>

<filter gitlab-event-listener.build>
  @type record_transformer
  keep_keys event.origin,event.name,git.repository.name,git.branch.name,git.commit.id,git.commit.message,git.commit.author.name,git.commit.author.email,ci.build.id,ci.build.stage,ci.build.status,ci.build.started_at,ci.build.finished_at,ci.build.duration,log
  renew_record
</filter>
